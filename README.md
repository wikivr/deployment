# Deployment of WikiVR on Kubernetes

## Requirements
- [kustomize](https://github.com/kubernetes-sigs/kustomize/blob/master/docs/INSTALL.md) for generating Kubernetes YAML files.
- [docker](https://docs.docker.com/docker-for-mac/install/) for running containers.
- [kind](https://github.com/kubernetes-sigs/kind) for using Kubernetes on Docker to test.
- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) for interacting with k8s.

## Deployment

- Create Kubernetes cluster using kind:
    ```
    kind create cluster --name wikivr
    ```

- Set your KUBECONFIG to wikivr k8s cluster
    ```
    KUBECONFIG=~/.kube/kind-config-wikivr
    ```

- Deploy the whole application to your k8s cluster:
    ```
    cd kubernetes
    kustomize build . | kubectl apply -f -
    ```

- Access the webpage by enabling port forwarding to your pod:
    ```
    kubectl port-forward <FRONTEND_PORT> 8000:8000 -n wikivr
    ```

- To delete the application, delete the `wikivr` namespace created in the previous step:
    ```
    kubectl delete ns wikivr
    ```

## Troubleshooting
_work in progress_
